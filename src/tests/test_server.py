import os

import requests

SERVER_HOST = os.environ.get('HSE_HTTP_TESTS_SERVER_HOST', 'server')
SERVER_PORT = int(os.environ.get('HSE_HTTP_FLASK_PORT', 80))
URL = 'http://' + SERVER_HOST
if SERVER_PORT != 80:
    URL += ':{}'.format(SERVER_PORT)


VALID_TASK1 = {
    'headline': 'Do my hw',
    'priority': 1,
    'deadline': '13.12.2020',
    'description': 'DS rest-http'
}

VALID_TASK2 = {
    'headline': 'Go to a shop',
    'priority': 2,
    'deadline': 'today',
    'description': 'To buy bread and milk'
}

VALID_TASK3 = {
    'headline': 'Send an email to the lecturer',
    'priority': 3,
    'deadline': '13.12.2020',
    'description': 'Ask about the exam'
}

INVALID_TASK = {
    'priority': 4,
    'deadline': 'next week',
}

def test_hello_world():
    response = requests.get(URL + '/')
    assert response.text == 'Hello, World!'
    assert response.status_code == 200

def test_add():
    response = requests.post(URL + '/add_task/', json=INVALID_TASK)
    assert response.json()['ADD'] == 'Task has no headline'
    assert response.status_code == 206

    response = requests.post(URL + '/add_task/', json=VALID_TASK1)
    assert response.json()['ADD'] == 'done'
    assert response.status_code == 200

def test_remove():
    response = requests.post(URL + '/add_task/', json=VALID_TASK2)
    task_id = response.json()['TASK_ID']

    response = requests.delete(URL + '/remove_task/' + str(task_id))
    assert response.status_code == 200

    response = requests.delete(URL + '/remove_task/' + str(task_id))
    assert response.status_code == 404

def test_get():
    response = requests.post(URL + '/add_task/', json=VALID_TASK1)
    response = requests.post(URL + '/add_task/', json=VALID_TASK2)
    response = requests.post(URL + '/add_task/', json=VALID_TASK3)

    response = requests.get(URL + '/get_tasks')
    assert response.status_code == 200

    # print(response.text)


def test_update():
    result = requests.post(URL + '/add_task/', json=VALID_TASK2)
    task_id = result.json()['TASK_ID']

    response = requests.put(URL + '/update_task/' + str(task_id), json={'description': 'Bonus HW'})
    assert response.status_code == 200

    response = requests.delete(URL + '/remove_task/' + str(task_id))
    assert response.status_code == 200

    response = requests.put(URL + '/update_task/' + str(task_id), json={'description': 'Bonus (+5) HW'})
    assert response.status_code == 404
