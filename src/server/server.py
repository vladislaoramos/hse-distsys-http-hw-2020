import os

from flask import Flask
from flask import jsonify, request

app = Flask(__name__)
TASK_LIST = []

class Task:
    def __init__(self, headline, priority, deadline, description):
        self.headline = headline
        self.priority = priority
        self.deadline = deadline
        self.description = description
        self.id = None


@app.route('/')
def hello_world():
    return 'Hello, World!'


@app.route('/add_task/', methods=['POST'])
def add_task():
    try:
        headline = request.json['headline']
        priority = int(request.json['priority'])
        deadline = request.json['deadline']
        description = request.json['description']
    except Exception:
        return jsonify({'ADD': 'Task has no headline'}), 206

    task = Task(headline, priority, deadline, description)
    TASK_LIST.append(task)
    task.id = len(TASK_LIST) - 1

    return jsonify({'ADD': 'done', 'TASK_ID': task.id}), 200


@app.route('/get_tasks/', methods=['GET'])
def get_tasks():
    resp = dict()
    for task in TASK_LIST:
        resp[task.id] = {
            'headline': task.headline,
            'priority': task.priority,
            'deadline': task.deadline,
            'description': task.description,
            'id': task.id
        }
    return jsonify(resp), 200


@app.route('/remove_task/<int:task_id>/', methods=['DELETE'])
def remove_task(task_id):
    try:
        del TASK_LIST[task_id]
        return jsonify({'DELETE': 'done'}), 200
    except Exception:
        return jsonify({'DELETE': 'Task is not found'}), 404


@app.route('/update_task/<int:task_id>/', methods=['PUT'])
def update_task(task_id):
    try:
        task = TASK_LIST[task_id]
    except Exception:
        return jsonify({'DELETE': 'Task is not found'}), 404

    if 'headline' in request.json:
        task.headline = request.json['headline']
    if 'priority' in request.json:
        task.priority = request.json['priority']
    if 'deadline' in request.json:
        task.deadline = int(request.json['deadline'])
    if 'description' in request.json:
        task.description = request.json['description']
    
    TASK_LIST[task_id] = task
    
    return jsonify({'UPDATE': 'done'}), 200


app.run(host='0.0.0.0', port=int(os.environ.get('HSE_HTTP_FLASK_PORT', 80)))
